#!/usr/bin/perl

use strict;
use warnings;
use v5.14;
use Time::HiRes qw/sleep/;
use Data::Dumper;

# debug
# $SIG{__WARN__} = sub { $DB::single = 1; CORE::warn(@_) };


# The Lisp Reader has a reader syntax for bytecode, it's #[...]
# src/lread.c:2560

# This syntax is stored as a vector.
# The role of each element (or slot) is given by enum Lisp_Compiled
# src/lisp.h:1639

use constant {
    # The prototype: nil, (a b), (a &rest b), ...
    ARGLIST     => 0,

    # The bytecode: a string storing a sequence of opcodes
    BYTECODE    => 1,

    # A vector of constants used in the bytecode: numbers, strings, symbols, ...
    CONSTANTS   => 2,

    # Maximum number of args used in the bytecode, counting the
    # calling function. The function and the args are pushed on the
    # stack before making the call so this is the size the stack must
    # be in order to handle all the calls of the bytecode (including
    # nested calls)
    STACK_DEPTH => 3,

    # Optional elements:

    # On old version of emacs, i think this used to be the
    # docstring itself. Now it's a cons where the car is the filename
    # of the compiled file and the cdr is the point position of the
    # docstring in this file.
    DOC_STRING  => 4,

    # When given, it indicated the function is interactive.
    # The value is the string passed to (interactive "xxx") when the
    # function was defined, or nil when none were given.
    INTERACTIVE => 5,
};

# opcode definition in src/bytecode.c
my @code;
# perl -nE 'print q{$code[}.$2.q{] = [0, q(}.$1.")];\n" if /DEFINE.*?B(.+?),.+?(\d+)/;' ~/prog/c/emacs/trunk/src/bytecode.c

use constant OP_STACK_REF => 0x00;
$code[OP_STACK_REF] = 'stack_ref';
use constant OP_STACK_REF1 => 0x01;
$code[OP_STACK_REF1] = 'stack_ref1';
use constant OP_STACK_REF2 => 0x02;
$code[OP_STACK_REF2] = 'stack_ref2';
use constant OP_STACK_REF3 => 0x03;
$code[OP_STACK_REF3] = 'stack_ref3';
use constant OP_STACK_REF4 => 0x04;
$code[OP_STACK_REF4] = 'stack_ref4';
use constant OP_STACK_REF5 => 0x05;
$code[OP_STACK_REF5] = 'stack_ref5';
use constant OP_STACK_REF6 => 0x06;
$code[OP_STACK_REF6] = 'stack_ref6';
use constant OP_STACK_REF7 => 0x07;
$code[OP_STACK_REF7] = 'stack_ref7';
use constant OP_VARREF => 0x08;
$code[OP_VARREF] = 'varref';
use constant OP_VARREF1 => 0x09;
$code[OP_VARREF1] = 'varref1';
use constant OP_VARREF2 => 0x0A;
$code[OP_VARREF2] = 'varref2';
use constant OP_VARREF3 => 0x0B;
$code[OP_VARREF3] = 'varref3';
use constant OP_VARREF4 => 0x0C;
$code[OP_VARREF4] = 'varref4';
use constant OP_VARREF5 => 0x0D;
$code[OP_VARREF5] = 'varref5';
use constant OP_VARREF6 => 0x0E;
$code[OP_VARREF6] = 'varref6';
use constant OP_VARREF7 => 0x0F;
$code[OP_VARREF7] = 'varref7';
use constant OP_VARSET => 0x10;
$code[OP_VARSET] = 'varset';
use constant OP_VARSET1 => 0x11;
$code[OP_VARSET1] = 'varset1';
use constant OP_VARSET2 => 0x12;
$code[OP_VARSET2] = 'varset2';
use constant OP_VARSET3 => 0x13;
$code[OP_VARSET3] = 'varset3';
use constant OP_VARSET4 => 0x14;
$code[OP_VARSET4] = 'varset4';
use constant OP_VARSET5 => 0x15;
$code[OP_VARSET5] = 'varset5';
use constant OP_VARSET6 => 0x16;
$code[OP_VARSET6] = 'varset6';
use constant OP_VARSET7 => 0x17;
$code[OP_VARSET7] = 'varset7';
use constant OP_VARBIND => 0x18;
$code[OP_VARBIND] = 'varbind';
use constant OP_VARBIND1 => 0x19;
$code[OP_VARBIND1] = 'varbind1';
use constant OP_VARBIND2 => 0x1A;
$code[OP_VARBIND2] = 'varbind2';
use constant OP_VARBIND3 => 0x1B;
$code[OP_VARBIND3] = 'varbind3';
use constant OP_VARBIND4 => 0x1C;
$code[OP_VARBIND4] = 'varbind4';
use constant OP_VARBIND5 => 0x1D;
$code[OP_VARBIND5] = 'varbind5';
use constant OP_VARBIND6 => 0x1E;
$code[OP_VARBIND6] = 'varbind6';
use constant OP_VARBIND7 => 0x1F;
$code[OP_VARBIND7] = 'varbind7';
use constant OP_CALL => 0x20;
$code[OP_CALL] = 'call';
use constant OP_CALL1 => 0x21;
$code[OP_CALL1] = 'call1';
use constant OP_CALL2 => 0x22;
$code[OP_CALL2] = 'call2';
use constant OP_CALL3 => 0x23;
$code[OP_CALL3] = 'call3';
use constant OP_CALL4 => 0x24;
$code[OP_CALL4] = 'call4';
use constant OP_CALL5 => 0x25;
$code[OP_CALL5] = 'call5';
use constant OP_CALL6 => 0x26;
$code[OP_CALL6] = 'call6';
use constant OP_CALL7 => 0x27;
$code[OP_CALL7] = 'call7';
use constant OP_UNBIND => 0x28;
$code[OP_UNBIND] = 'unbind';
use constant OP_UNBIND1 => 0x29;
$code[OP_UNBIND1] = 'unbind1';
use constant OP_UNBIND2 => 0x2A;
$code[OP_UNBIND2] = 'unbind2';
use constant OP_UNBIND3 => 0x2B;
$code[OP_UNBIND3] = 'unbind3';
use constant OP_UNBIND4 => 0x2C;
$code[OP_UNBIND4] = 'unbind4';
use constant OP_UNBIND5 => 0x2D;
$code[OP_UNBIND5] = 'unbind5';
use constant OP_UNBIND6 => 0x2E;
$code[OP_UNBIND6] = 'unbind6';
use constant OP_UNBIND7 => 0x2F;
$code[OP_UNBIND7] = 'unbind7';
use constant OP_NTH => 0x38;
$code[OP_NTH] = 'nth';
use constant OP_SYMBOLP => 0x39;
$code[OP_SYMBOLP] = 'symbolp';
use constant OP_CONSP => 0x3A;
$code[OP_CONSP] = 'consp';
use constant OP_STRINGP => 0x3B;
$code[OP_STRINGP] = 'stringp';
use constant OP_LISTP => 0x3C;
$code[OP_LISTP] = 'listp';
use constant OP_EQ => 0x3D;
$code[OP_EQ] = 'eq';
use constant OP_MEMQ => 0x3E;
$code[OP_MEMQ] = 'memq';
use constant OP_NOT => 0x3F;
$code[OP_NOT] = 'not';
use constant OP_CAR => 0x40;
$code[OP_CAR] = 'car';
use constant OP_CDR => 0x41;
$code[OP_CDR] = 'cdr';
use constant OP_CONS => 0x42;
$code[OP_CONS] = 'cons';
use constant OP_LIST1 => 0x43;
$code[OP_LIST1] = 'list1';
use constant OP_LIST2 => 0x44;
$code[OP_LIST2] = 'list2';
use constant OP_LIST3 => 0x45;
$code[OP_LIST3] = 'list3';
use constant OP_LIST4 => 0x46;
$code[OP_LIST4] = 'list4';
use constant OP_LENGTH => 0x47;
$code[OP_LENGTH] = 'length';
use constant OP_AREF => 0x48;
$code[OP_AREF] = 'aref';
use constant OP_ASET => 0x49;
$code[OP_ASET] = 'aset';
use constant OP_SYMBOL_VALUE => 0x4A;
$code[OP_SYMBOL_VALUE] = 'symbol_value';
use constant OP_SYMBOL_FUNCTION => 0x4B;
$code[OP_SYMBOL_FUNCTION] = 'symbol_function';
use constant OP_SET => 0x4C;
$code[OP_SET] = 'set';
use constant OP_FSET => 0x4D;
$code[OP_FSET] = 'fset';
use constant OP_GET => 0x4E;
$code[OP_GET] = 'get';
use constant OP_SUBSTRING => 0x4F;
$code[OP_SUBSTRING] = 'substring';
use constant OP_CONCAT2 => 0x50;
$code[OP_CONCAT2] = 'concat2';
use constant OP_CONCAT3 => 0x51;
$code[OP_CONCAT3] = 'concat3';
use constant OP_CONCAT4 => 0x52;
$code[OP_CONCAT4] = 'concat4';
use constant OP_SUB1 => 0x53;
$code[OP_SUB1] = 'sub1';
use constant OP_ADD1 => 0x54;
$code[OP_ADD1] = 'add1';
use constant OP_EQLSIGN => 0x55;
$code[OP_EQLSIGN] = 'eqlsign';
use constant OP_GTR => 0x56;
$code[OP_GTR] = 'gtr';
use constant OP_LSS => 0x57;
$code[OP_LSS] = 'lss';
use constant OP_LEQ => 0x58;
$code[OP_LEQ] = 'leq';
use constant OP_GEQ => 0x59;
$code[OP_GEQ] = 'geq';
use constant OP_DIFF => 0x5A;
$code[OP_DIFF] = 'diff';
use constant OP_NEGATE => 0x5B;
$code[OP_NEGATE] = 'negate';
use constant OP_PLUS => 0x5C;
$code[OP_PLUS] = 'plus';
use constant OP_MAX => 0x5D;
$code[OP_MAX] = 'max';
use constant OP_MIN => 0x5E;
$code[OP_MIN] = 'min';
use constant OP_MULT => 0x5F;
$code[OP_MULT] = 'mult';
use constant OP_POINT => 0x60;
$code[OP_POINT] = 'point';
use constant OP_SAVE_CURRENT_BUFFER => 0x61;
$code[OP_SAVE_CURRENT_BUFFER] = 'save_current_buffer';
use constant OP_GOTO_CHAR => 0x62;
$code[OP_GOTO_CHAR] = 'goto_char';
use constant OP_INSERT => 0x63;
$code[OP_INSERT] = 'insert';
use constant OP_POINT_MAX => 0x64;
$code[OP_POINT_MAX] = 'point_max';
use constant OP_POINT_MIN => 0x65;
$code[OP_POINT_MIN] = 'point_min';
use constant OP_CHAR_AFTER => 0x66;
$code[OP_CHAR_AFTER] = 'char_after';
use constant OP_FOLLOWING_CHAR => 0x67;
$code[OP_FOLLOWING_CHAR] = 'following_char';
use constant OP_PRECEDING_CHAR => 0x68;
$code[OP_PRECEDING_CHAR] = 'preceding_char';
use constant OP_CURRENT_COLUMN => 0x69;
$code[OP_CURRENT_COLUMN] = 'current_column';
use constant OP_INDENT_TO => 0x6A;
$code[OP_INDENT_TO] = 'indent_to';
use constant OP_EOLP => 0x6C;
$code[OP_EOLP] = 'eolp';
use constant OP_EOBP => 0x6D;
$code[OP_EOBP] = 'eobp';
use constant OP_BOLP => 0x6E;
$code[OP_BOLP] = 'bolp';
use constant OP_BOBP => 0x6F;
$code[OP_BOBP] = 'bobp';
use constant OP_CURRENT_BUFFER => 0x70;
$code[OP_CURRENT_BUFFER] = 'current_buffer';
use constant OP_SET_BUFFER => 0x71;
$code[OP_SET_BUFFER] = 'set_buffer';
use constant OP_SAVE_CURRENT_BUFFER_1 => 0x72;
$code[OP_SAVE_CURRENT_BUFFER_1] = 'save_current_buffer_1';
use constant OP_INTERACTIVE_P => 0x74;
$code[OP_INTERACTIVE_P] = 'interactive_p';
use constant OP_FORWARD_CHAR => 0x75;
$code[OP_FORWARD_CHAR] = 'forward_char';
use constant OP_FORWARD_WORD => 0x76;
$code[OP_FORWARD_WORD] = 'forward_word';
use constant OP_SKIP_CHARS_FORWARD => 0x77;
$code[OP_SKIP_CHARS_FORWARD] = 'skip_chars_forward';
use constant OP_SKIP_CHARS_BACKWARD => 0x78;
$code[OP_SKIP_CHARS_BACKWARD] = 'skip_chars_backward';
use constant OP_FORWARD_LINE => 0x79;
$code[OP_FORWARD_LINE] = 'forward_line';
use constant OP_CHAR_SYNTAX => 0x7A;
$code[OP_CHAR_SYNTAX] = 'char_syntax';
use constant OP_BUFFER_SUBSTRING => 0x7B;
$code[OP_BUFFER_SUBSTRING] = 'buffer_substring';
use constant OP_DELETE_REGION => 0x7C;
$code[OP_DELETE_REGION] = 'delete_region';
use constant OP_NARROW_TO_REGION => 0x7D;
$code[OP_NARROW_TO_REGION] = 'narrow_to_region';
use constant OP_WIDEN => 0x7E;
$code[OP_WIDEN] = 'widen';
use constant OP_END_OF_LINE => 0x7F;
$code[OP_END_OF_LINE] = 'end_of_line';
use constant OP_CONSTANT2 => 0x81;
$code[OP_CONSTANT2] = 'constant2';
use constant OP_GOTO => 0x82;
$code[OP_GOTO] = 'goto';
use constant OP_GOTOIFNIL => 0x83;
$code[OP_GOTOIFNIL] = 'gotoifnil';
use constant OP_GOTOIFNONNIL => 0x84;
$code[OP_GOTOIFNONNIL] = 'gotoifnonnil';
use constant OP_GOTOIFNILELSEPOP => 0x85;
$code[OP_GOTOIFNILELSEPOP] = 'gotoifnilelsepop';
use constant OP_GOTOIFNONNILELSEPOP => 0x86;
$code[OP_GOTOIFNONNILELSEPOP] = 'gotoifnonnilelsepop';
use constant OP_RETURN => 0x87;
$code[OP_RETURN] = 'return';
use constant OP_DISCARD => 0x88;
$code[OP_DISCARD] = 'discard';
use constant OP_DUP => 0x89;
$code[OP_DUP] = 'dup';
use constant OP_SAVE_EXCURSION => 0x8A;
$code[OP_SAVE_EXCURSION] = 'save_excursion';
use constant OP_SAVE_WINDOW_EXCURSION => 0x8B;
$code[OP_SAVE_WINDOW_EXCURSION] = 'save_window_excursion';
use constant OP_SAVE_RESTRICTION => 0x8C;
$code[OP_SAVE_RESTRICTION] = 'save_restriction';
use constant OP_CATCH => 0x8D;
$code[OP_CATCH] = 'catch';
use constant OP_UNWIND_PROTECT => 0x8E;
$code[OP_UNWIND_PROTECT] = 'unwind_protect';
use constant OP_CONDITION_CASE => 0x8F;
$code[OP_CONDITION_CASE] = 'condition_case';
use constant OP_TEMP_OUTPUT_BUFFER_SETUP => 0x90;
$code[OP_TEMP_OUTPUT_BUFFER_SETUP] = 'temp_output_buffer_setup';
use constant OP_TEMP_OUTPUT_BUFFER_SHOW => 0x91;
$code[OP_TEMP_OUTPUT_BUFFER_SHOW] = 'temp_output_buffer_show';
use constant OP_UNBIND_ALL => 0x92;
$code[OP_UNBIND_ALL] = 'unbind_all';
use constant OP_SET_MARKER => 0x93;
$code[OP_SET_MARKER] = 'set_marker';
use constant OP_MATCH_BEGINNING => 0x94;
$code[OP_MATCH_BEGINNING] = 'match_beginning';
use constant OP_MATCH_END => 0x95;
$code[OP_MATCH_END] = 'match_end';
use constant OP_UPCASE => 0x96;
$code[OP_UPCASE] = 'upcase';
use constant OP_DOWNCASE => 0x97;
$code[OP_DOWNCASE] = 'downcase';
use constant OP_STRINGEQLSIGN => 0x98;
$code[OP_STRINGEQLSIGN] = 'stringeqlsign';
use constant OP_STRINGLSS => 0x99;
$code[OP_STRINGLSS] = 'stringlss';
use constant OP_EQUAL => 0x9A;
$code[OP_EQUAL] = 'equal';
use constant OP_NTHCDR => 0x9B;
$code[OP_NTHCDR] = 'nthcdr';
use constant OP_ELT => 0x9C;
$code[OP_ELT] = 'elt';
use constant OP_MEMBER => 0x9D;
$code[OP_MEMBER] = 'member';
use constant OP_ASSQ => 0x9E;
$code[OP_ASSQ] = 'assq';
use constant OP_NREVERSE => 0x9F;
$code[OP_NREVERSE] = 'nreverse';
use constant OP_SETCAR => 0xA0;
$code[OP_SETCAR] = 'setcar';
use constant OP_SETCDR => 0xA1;
$code[OP_SETCDR] = 'setcdr';
use constant OP_CAR_SAFE => 0xA2;
$code[OP_CAR_SAFE] = 'car_safe';
use constant OP_CDR_SAFE => 0xA3;
$code[OP_CDR_SAFE] = 'cdr_safe';
use constant OP_NCONC => 0xA4;
$code[OP_NCONC] = 'nconc';
use constant OP_QUO => 0xA5;
$code[OP_QUO] = 'quo';
use constant OP_REM => 0xA6;
$code[OP_REM] = 'rem';
use constant OP_NUMBERP => 0xA7;
$code[OP_NUMBERP] = 'numberp';
use constant OP_INTEGERP => 0xA8;
$code[OP_INTEGERP] = 'integerp';
use constant OP_RGOTO => 0xAA;
$code[OP_RGOTO] = 'Rgoto';
use constant OP_RGOTOIFNIL => 0xAB;
$code[OP_RGOTOIFNIL] = 'Rgotoifnil';
use constant OP_RGOTOIFNONNIL => 0xAC;
$code[OP_RGOTOIFNONNIL] = 'Rgotoifnonnil';
use constant OP_RGOTOIFNILELSEPOP => 0xAD;
$code[OP_RGOTOIFNILELSEPOP] = 'Rgotoifnilelsepop';
use constant OP_RGOTOIFNONNILELSEPOP => 0xAE;
$code[OP_RGOTOIFNONNILELSEPOP] = 'Rgotoifnonnilelsepop';
use constant OP_LISTN => 0xAF;
$code[OP_LISTN] = 'listN';
use constant OP_CONCATN => 0xB0;
$code[OP_CONCATN] = 'concatN';
use constant OP_INSERTN => 0xB1;
$code[OP_INSERTN] = 'insertN';
use constant OP_STACK_SET => 0xB2;
$code[OP_STACK_SET] = 'stack_set';
use constant OP_STACK_SET2 => 0xB3;
$code[OP_STACK_SET2] = 'stack_set2';
use constant OP_DISCARDN => 0xB6;
$code[OP_DISCARDN] = 'discardN';
use constant OP_CONSTANT => 0xC0;
$code[OP_CONSTANT] = 'constant';

use constant {
    T_EOF        => 0,
    T_LIST_START => 1,
    T_LIST_END   => 2,
    T_ATOM       => 3,
    T_QUOTE      => 4,
    T_BYTECODE   => 5,
    T_NUMFORM_SET => 6,
    T_NUMFORM_REF => 7,
    T_QUOTE      => 8,
};

my @tok_name;
$tok_name[T_EOF] = "T_EOF";
$tok_name[T_LIST_START] = "T_LIST_START";
$tok_name[T_LIST_END] = "T_LIST_END";
$tok_name[T_ATOM] = "T_ATOM";
$tok_name[T_QUOTE] = "T_QUOTE";
$tok_name[T_BYTECODE] = "T_BYTECODE";
$tok_name[T_NUMFORM_SET] = "T_NUMFORM_SET";
$tok_name[T_NUMFORM_REF] = "T_NUMFORM_REF";
$tok_name[T_QUOTE] = "T_QUOTE";


{
    # static var
    my $buf;
    my $off = 0;
    my @res;

    sub init {
        # binmode <>;
        $buf = join('', <>);
    }

    sub readrx {
        my $rx = shift;
        return () if $off >= length($buf);
        my $s = substr($buf, $off);
        @res = ($s =~ /^$rx/);
        if(@res) {
            $off += $+[0];
        }
        return @res;
    }

    sub tokenize {
        init() if !$buf;
        {
            return (T_EOF) if($off >= length($buf));

            # skip stuff
            redo if readrx(qr/\s+|;.*\n/);

            if(readrx(qr/#@(\d+) /)) {
                $off += $res[0];
                redo;
            }

            # uninterned symbols are just symbols
            redo if readrx(qr/#:/);

            if(readrx(qr/#\[/)) {
                return (T_BYTECODE, '#[');
            }

            return (T_ATOM, 'xxx-empty-sym') if(readrx(qr/##/));
            return (T_ATOM, 'xxx-file-name') if(readrx(qr/#\$/));

            if(readrx(qr/#(\d+)=/)) {
                return (T_NUMFORM_SET, $res[0]);
            }

            if(readrx(qr/#(\d+)#/)) {
                return (T_NUMFORM_REF, $res[0]);
            }

            # XXX: cons are parsed as list and "." is a symbol
            return (T_QUOTE) if readrx(qr/'/);
            return (T_LIST_START, $res[0]) if readrx(qr/([\(|\[])/);
            return (T_LIST_END, $res[0]) if readrx(qr/([\)|\]])/);
            return (T_ATOM, $res[0]) if readrx(qr/("(?:[^\\"]|\\.)*")/);
            return (T_ATOM, $res[0]) if readrx(qr/([^#\(\)\[\]\'\s]+)/);

            my $s = substr($buf, $off, 20);
            $s =~ s/\n/\\n/g;
            die "at [$s]...\n";
        }
    }
}

{
    my @numform;

    sub parse {
        my @list = ();

        while(1) {
            my ($tok, $val) = tokenize;
            my $type;

            return \@list if $tok == T_LIST_END || $tok == T_EOF;


            if ($tok == T_LIST_START || $tok == T_BYTECODE) {
                $type = $val;
                $val = parse();
            }

            elsif ($tok == T_NUMFORM_REF) {
                ($tok, $val, $type) = @{$numform[$val]};
            }

            elsif ($tok == T_NUMFORM_SET) {
                my $n = $val;
                ($tok, $val) = tokenize;

                if($tok == T_LIST_START || $tok == T_BYTECODE) {
                    $type = $val;
                    $val = parse();
                }

                $numform[$n] = [$tok, $val, $type];
            }

            elsif ($tok == T_QUOTE) {
                ($tok, $val) = tokenize;

                if($tok == T_LIST_START || $tok == T_BYTECODE) {
                    $type = $val;
                    $val = [$tok, parse(), $type];
                } else {
                    $val = [$tok, $val];
                }
                $tok = T_QUOTE;
            }

            push @list, [$tok, $val, $type];
        }
    }
}

sub bc_to_int {
    my $s = shift;
    $s =~ s/^"|"$//g;
    $s =~ s/\\n/\n/g;
    $s =~ s/\\f/\f/g;
    $s =~ s/\\"/"/g;
    $s =~ s/\\\\/\\/g;
    $s =~ s/\\([0-7]{1,3})/chr(oct($1))/eg;
    return map {ord} split(//, $s);
}

sub exp_to_string {
    my $e = shift;
    given($e->[0]) {
        when(T_ATOM) {
            return $e->[1];
        }
        when(T_LIST_START) {
            my $type = $e->[2];
            my $s = join(' ', map {exp_to_string($_)} @{$e->[1]});
            if($type eq '(') {
                return "($s)";
            } else {
                return "[$s]";
            }
        }
        when(T_BYTECODE) {
            return '#['.join(' ', map {exp_to_string($_)} @{$e->[1]}).']';
        }
        when(T_QUOTE) {
            return "'".exp_to_string($e->[1]);
        }
    }
    die "unknow type ".$e->[0];
}

sub find_bc {
    my $list = shift;
    my @res;

    # catch top level (defalias & (byte-code
    for my $e (@{$list}) {
        my ($fname, $args, $bc, $const);
        my ($t, $v) = @{$e};
        next if $t != T_LIST_START;
        my $sym = $v->[0];
        next if $sym->[0] != T_ATOM;

        if($sym->[1] eq 'defalias' && $v->[2][0] == T_BYTECODE) {
            my $array = $v->[2][1];

            $fname = $v->[1][1][1];
            $args  = $array->[0][0] == T_ATOM ? '()' : exp_to_string($array->[0]);
            $bc    = $array->[1][1];
            $const = [map {exp_to_string($_)} @{$array->[2][1]}];
        }

        elsif($sym->[1] eq 'byte-code') {
            $bc = $v->[1][1];
            $const = [map {exp_to_string($_)} @{$v->[2][1]}];
        }

        if($bc && $const) {
            push @res, [$fname, $args, $bc, $const];
        }
    }
    return @res;
}

sub disass {
    my @bc = @{shift()};
    my @cst = @{shift()};

    my ($op, $off, $i);

    local *fetch = sub {
        $bc[++$i];
    };

    local *fetch2 = sub {
        fetch() + (fetch() << 8);
    };

    local *multi_op_val = sub {
        my ($min, $c, $max, $double) = @_;
        my $val;
        if($double && $c == $max) {
            $val = fetch2();
        }
        elsif($double && $c == $max-1) {
            $val = fetch();
        }
        elsif($c == $max) {
            $val = fetch();
        } else {
            $val = $c - $min;
        }
        return $val;
    };

    local *p = sub {
        my ($n, $v) = ($code[$op], "");
        if(@_ >= 2) {
            ($n, $v) = @_;
        } elsif(@_ == 1) {
            ($v) = @_;
        }
        printf "%3d:  %-12.12s %s\n", $off, $n, $v;
    };


    for($i = 0; $i < @bc; $i++) {
        $op = $bc[$i];
        $off = $i;


        given($op) {
            when(OP_VARBIND <= $_&&$_ <= OP_VARBIND7) {
                p($cst[multi_op_val(OP_VARBIND, $_, OP_VARBIND7, 1)]);
            }

            when(OP_VARREF <= $_&&$_ <= OP_VARREF7) {
                p($cst[multi_op_val(OP_VARREF, $_, OP_VARREF7, 1)]);
            }

            when(OP_VARSET <= $_&&$_ <= OP_VARSET7) {
                p(multi_op_val(OP_VARSET, $_, OP_VARSET7, 1));
            }

            when(OP_CALL <= $_&&$_ <= OP_CALL7) {
                p(multi_op_val(OP_CALL, $_, OP_CALL7, 1));
            }

            when(OP_UNBIND <= $_&&$_ <= OP_UNBIND7) {
                p(multi_op_val(OP_UNBIND, $_, OP_UNBIND7, 1));
            }

            when($_ ~~ [OP_GOTO, OP_GOTOIFNIL, OP_GOTOIFNONNIL,
                        OP_GOTOIFNILELSEPOP, OP_GOTOIFNONNILELSEPOP])
            {
                p(fetch2());
            }

            when(OP_RGOTO) {
                p(fetch()-127);
            }

            when($_ ~~ [OP_RGOTOIFNIL, OP_RGOTOIFNONNIL,
                        OP_RGOTOIFNILELSEPOP, OP_RGOTOIFNONNILELSEPOP])
            {
                p(fetch()-128);
            }

            when($_ ~~ [OP_LISTN, OP_CONCATN, OP_INSERTN, OP_DISCARDN]) {
                p(fetch());
            }

            when(OP_STACK_SET) {
                p('stackset', fetch());
            }

            when(OP_STACK_SET) {
                p(fetch2());
            }

            when(OP_STACK_REF) {
                say "invalid";
            }

            when(OP_STACK_REF1 <=$_&&$_<= OP_STACK_REF7) {
                p(multi_op_val(OP_STACK_REF1, $_, OP_STACK_REF7, 1));
            }

            when(OP_CONSTANT2) {
                p($cst[fetch2()]);
            }

            when(!defined $code[$_] && $_ < OP_CONSTANT) {
                say "undef.";
            }

            when($_ >= OP_CONSTANT) {
                p('const', $cst[$_ - OP_CONSTANT]);
            }

            default {
                p();
            }
        }
    }
}

sub main {
    my @bc_list = find_bc(parse());
    for(@bc_list) {
        my ($fn, $arg, $bc, $cst) = @$_;
        if($fn) {
            print
                "function: $fn\n",
                "args:     $arg\n";
        } else {
            print "raw bytecode:\n";
        }
        disass([bc_to_int($bc)], $cst);
        print "\n\n";
    }

}

# for debugging...
sub dump_func_bytecode {
    my @bc_list = find_bc(parse());
    for(@bc_list) {
        next if !$_->[0];
        say $_->[0]."\t". join(" ", bc_to_int($_->[2]));
    }
}

main();
#dump_func_bytecode();
